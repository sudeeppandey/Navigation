package pandey.sudeep.navigation;

/**
 * Created by Sudeep.Pandey on 3/9/2018.
 */

public class MyCarrier {

    private int imageResourceID;
    private String name;

    public MyCarrier(String _name, int _imageResourceID){
        this.imageResourceID = _imageResourceID;
        this.name = _name;
    }

    public int getImageResourceID(){
        return imageResourceID;
    }

    public String getName(){
        return name;
    }

}
