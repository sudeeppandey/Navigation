package pandey.sudeep.navigation;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends FragmentActivity {

    DemoCollectionPagerAdapter mDemoCollectionPagerAdapter;
    ViewPager mViewPager;
    private List<MyCarrier> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        myToolbar.setTitle("Swipe Card");
        //myToolbar.setSubtitle("implementing effective navigation");

        data = new ArrayList<MyCarrier>();

        data.add(0,new MyCarrier("Barcelona",R.drawable.barcelona));
        data.add(1,new MyCarrier("Cordoba",R.drawable.cordoba));
        data.add(2,new MyCarrier("Gran Canaria",R.drawable.gran_canaria));
        data.add(3,new MyCarrier("Granada",R.drawable.granada));
        data.add(4,new MyCarrier("Ibiza",R.drawable.ibiza));
        data.add(5,new MyCarrier("Lanzarote",R.drawable.lanzarote));
        data.add(6,new MyCarrier("Madrid",R.drawable.madrid));
        data.add(7,new MyCarrier("Balearic",R.drawable.balearic));
        data.add(8,new MyCarrier("Majorca",R.drawable.majorca));
        data.add(9,new MyCarrier("Malaga",R.drawable.malaga));
        data.add(10,new MyCarrier("Palma",R.drawable.palma));
        data.add(11,new MyCarrier("Sevilla",R.drawable.sevilla));
        data.add(12,new MyCarrier("Teida",R.drawable.teide));
        data.add(13,new MyCarrier("Tenerife",R.drawable.tenerife));
        data.add(14,new MyCarrier("Valencia",R.drawable.valencia));


        mDemoCollectionPagerAdapter =
                new DemoCollectionPagerAdapter(
                        getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mDemoCollectionPagerAdapter);

    }

    public class DemoCollectionPagerAdapter extends FragmentStatePagerAdapter {
        public DemoCollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new DemoObjectFragment();
            Bundle args = new Bundle();

            args.putString(DemoObjectFragment.FIELD_1, data.get(i).getName());
            args.putInt(DemoObjectFragment.FIELD_2, data.get(i).getImageResourceID());

            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "City " + (position + 1);
        }
    }

    public static class DemoObjectFragment extends Fragment {
        public static final String FIELD_1 = "Spanish_city";
        public static final String FIELD_2 = "Image_ID";

        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(
                    R.layout.mycard, container, false);

            Bundle args = getArguments();

            ((ImageView) rootView.findViewById(R.id.theCard).findViewById(R.id.layout).findViewById(R.id.imageView)).setImageResource(
                    args.getInt(FIELD_2));

            ((TextView) rootView.findViewById(R.id.theCard).findViewById(R.id.layout).findViewById(R.id.textView)).setText(
                    args.getString(FIELD_1));

            return rootView;
        }
    }
}
